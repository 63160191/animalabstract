/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TestAniamal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.speak();
        System.out.println("h1 is Animal? " + (h1 instanceof Animal));
        System.out.println("h1 is LandAnimal? " + (h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is LandAnimal? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile? " + (a1 instanceof Reptile));
        
        Cat c1 = new Cat("Puma");
        c1.run();
        c1.sleep();
        System.out.println("c1 is Animal? " + (c1 instanceof Animal));
        System.out.println("c1 is LandAnimal? " + (c1 instanceof LandAnimal));
        
        Dog d1 = new Dog("Panter");
        d1.eat();
        d1.walk();
        d1.speak();
        d1.run();
        d1.sleep();
        System.out.println("d1 is Dog? " + (d1 instanceof Dog));
        
        Crocodile cro1 = new Crocodile("CoCo");
        cro1.crawl();
        cro1.sleep();
        cro1.walk();
        System.out.println("cro1 is Animal? " + (cro1 instanceof Animal));
        System.out.println("cro1 is Crocodile? " + (cro1 instanceof Crocodile));
        
        Snake s1 = new Snake("Shok");
        s1.crawl();
        s1.walk();
        s1.speak();
        s1.eat();
        System.out.println("s1 is Animal? " + (s1 instanceof Animal));
        System.out.println("s1 is Snake? " + (s1 instanceof Snake));
        Animal a2 = s1;
        System.out.println("a2 is Crocodile? " + (a2 instanceof Crocodile));
        
        Fish f1 = new Fish("Pop");
        f1.swim();
        f1.speak();
        f1.eat();
        f1.sleep();
        System.out.println("f1 is Fish? " + (f1 instanceof Fish));
        
        Crab crab1 = new Crab("Babo");
        crab1.walk();
        crab1.eat();
        crab1.speak();
        System.out.println("crab1 is AquaticAnimal? " + (crab1 instanceof AquaticAnimal));
        System.out.println("crab1 is Crab? " + (crab1 instanceof Crab));
        
        Bat bat1 = new Bat("Black");
        bat1.eat();
        bat1.fly();
        bat1.speak();
        bat1.sleep();
        System.out.println("bat1 is Bat? " + (bat1 instanceof Bat));
        System.out.println("bat1 is Animal? " + (bat1 instanceof Animal));
        System.out.println("bat1 is Poulty? " + (bat1 instanceof Poulty));
        
        Bird bi1 = new Bird("Poppy");
        bi1.eat();
        bi1.fly();
        bi1.speak();
        bi1.sleep();
        bi1.walk();
        System.out.println("bi1 is Bird? " + (bi1 instanceof Bird));
        System.out.println("bi1 is Animal? " + (bi1 instanceof Animal));
        System.out.println("bi1 is Poulty? " + (bi1 instanceof Poulty));
        
        Animal a3 = bi1;
        System.out.println("a3 is LandAnimal? " + (a3 instanceof LandAnimal));
        
    }
}
